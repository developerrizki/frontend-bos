<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>@yield('title') - Jaringan BOS</title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>

<body>

    <div class="wrapper">
        <!-- Sidebar Holder -->
        <nav id="sidebar">
            <div class="sidebar-header text-center">
                <img src="{{ asset('images/user.png') }}" alt="User" width="80px">
                <h3>{{ ucwords(strtolower( Session::get('auth')['name'])) }}</h3>
                <span>{{ Session::get('auth')['phone'] }}</span>
            </div>

            <ul class="list-unstyled components">
                <p>Main Menu</p>
                <li @if(Request::is('dasbor'))  class="active" @endif>
                    <a href="{{ url('dasbor') }}">Dasbor</a>
                </li>
                <li @if(Request::is('tim'))  class="active" @endif>
                    <a href="{{ url('tim') }}">Tim Saya</a>
                </li>
                <li @if(Request::is('bonus'))  class="active" @endif>
                    <a href="{{ url('bonus?periode='. date('Y-m-d') .'&filter=now') }}">Bonus & Komisi</a>
                </li>
                <br>
                <p>Akun Saya</p>
                <li @if(Request::is('profil'))  class="active" @endif>
                    <a href="{{ url('profil') }}">Ubah Profil</a>
                </li>
                <li>
                    <a href="{{ url('logout') }}">Keluar</a>
                </li>
            </ul>
        </nav>

        <!-- Page Content Holder -->
        <div id="content">

            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="navbar-btn">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                    
                    <div class="nav navbar-nav ml-auto">
                        <img src="{{ asset('images/logo-bos.jpg') }}" alt="Logo BOS" width="125px">
                    </div>
                </div>
            </nav>
            
            @yield('content')
        </div>
    </div>

    <!-- jQuery -->
    <script src="{{ asset('js/jquery.js') }}"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
                $(this).toggleClass('active');
            });
        });
    </script>
</body>

</html>