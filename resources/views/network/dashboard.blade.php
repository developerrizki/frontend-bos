@extends('layouts.root')

@section('title', 'DASBOR')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h5>Detil Profil</h5> <br>
            <div class="table-responsive">
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <td width="20%">Jenis Kelamin</td>
                            <td width="2%">:</td>
                            <td>{{ $profile->user_gender == "L" ? "Laki-laki" : "Perempuan" }}</td>
                        </tr>
                        <tr>
                            <td width="20%">Email</td>
                            <td width="2%">:</td>
                            <td>{{ $profile->user_email }}</td>
                        </tr>
                        <tr>
                            <td width="20%">Alamat</td>
                            <td width="2%">:</td>
                            <td>{{ $profile->user_address }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-sm-12">
            <br> <h5>Detil Akun</h5> <br>
            <div class="table-responsive">
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <td width="20%">Username</td>
                            <td width="2%">:</td>
                            <td>{{ $profile->user_username }}</td>
                        </tr>
                        <tr>
                            <td width="20%">Saldo</td>
                            <td width="2%">:</td>
                            <td>Rp. {{ number_format($profile->user_balance) }}</td>
                        </tr>
                        <tr>
                            <td width="20%">Poin</td>
                            <td width="2%">:</td>
                            <td>{{ $profile->user_point }}</td>
                        </tr>
                        <tr>
                            <td width="20%">Sponsor</td>
                            <td width="2%">:</td>
                            <td>{{ !empty($upline) ? $upline->user_name : "-" }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection