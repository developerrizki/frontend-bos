@extends('layouts.root')

@section('title', 'BONUS & KOMISI')

@section('content')
    <div class="row mb-3">
        <div class="col-sm-6 offset-sm-6">
            <div class="card">
                <div class="card-body text-center" style="padding:10px;">
                    <h5 class="card-title">Total Bonus & Komisi</h5>
                    <span class="text-success text-bold">Rp. {{ number_format($total) }}</span>
                </div>
            </div>
        </div>
    </div>

    <div class="row mb-4">
        <div class="col-sm-3">
            <a href="{{ url('bonus?periode='.$periode['now'].'&filter=now') }}" class="card {{ Request::get('filter') == "now" ? "card-active" : "" }} card-bos">
                <div class="card-body text-center" style="padding:10px;">
                    Hari Ini
                </div>
            </a>
        </div>
        <div class="col-sm-3">
            <a href="{{ url('bonus?periode='.$periode['sevenDay'].'&filter=sevenDay') }}" class="card {{ Request::get('filter') == "sevenDay" ? "card-active" : "" }} card-bos">
                <div class="card-body text-center" style="padding:10px;">
                    7 Hari
                </div>
            </a>
        </div>
        <div class="col-sm-3">
            <a href="{{ url('bonus?periode='.$periode['oneMonth'].'&filter=oneMonth') }}" class="card {{ Request::get('filter') == "oneMonth" ? "card-active" : "" }} card-bos">
                <div class="card-body text-center" style="padding:10px;">
                    1 Bulan
                </div>
            </a>
        </div>
        <div class="col-sm-3">
            <a href="{{ url('bonus?periode='.$periode['threeMonth'].'&filter=threeMonth') }}" class="card {{ Request::get('filter') == "threeMonth" ? "card-active" : "" }} card-bos">
                <div class="card-body text-center" style="padding:10px;">
                    3 Bulan
                </div>
            </a>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-12">
            <h6>Riwayat Bonus & Komisi</h6>
            <div class="table-responsive mt-3">
                <table class="table table-striped" style="font-size: 12px">
                    <thead>
                        <th width="5%">No</th>
                        <th width="20%">Kode Transaksi</th>
                        <th width="20%">Tanggal</th>
                        <th width="45%">Deskripsi</th>
                        <th width="10%">Bonus</th>
                    </thead>
                    <tbody>
                        @if(sizeof($history) > 0)
                            @foreach ($history as $key => $result)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $result->transaction_id }}</td>
                                    <td>{{ date('d M Y H:i:s', strtotime($result->created_time)) }}</td>
                                    <td>{{ $result->transaction_desc }}</td>
                                    <td><div class="text-success">+ Rp. 5</div></td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5">Tidak ada bonus & komisi.</td>
                            </tr>
                        @endif
                    </tbody>
                    {{-- <tfoot>
                        <tr>
                            <td colspan="4">{{ $history->appends(['periode' => Request::get('periode'), 'filter' => Request::get('filter')])->links() }}</td>
                        </tr>
                    </tfoot> --}}
                </table>
            </div>
        </div>
    </div>
@endsection