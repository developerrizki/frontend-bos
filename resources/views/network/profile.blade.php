@extends('layouts.root')

@section('title', 'PROFIL')

@section('content')
    {{-- ALERT --}}
    @if(Session::has('status') && Session::get('status') == "success")
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>Berhasil </strong> {{ Session::get('msg') }}
    </div>
    @endif

    <div class="row">
        <div class="col-sm-12">
            <h5>Ubah Profil</h5>
            <div class="line"></div>
            
            <form action="{{ url('profil') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="" class="control-label">Nama</label>
                    <input type="text" class="form-control" name="user_name" id="user_name" disabled value="{{ $profile->user_name }}">
                    <span class="text-danger">* Nama hanya bisa diubah melalui aplikasi.</span>
                </div>
                <div class="form-group">
                    <label for="" class="control-label">Email</label>
                    <input type="text" class="form-control" name="user_email" id="user_email" value="{{ $profile->user_email }}" placeholder="Masukan email aktif Anda">
                </div>
                <div class="form-group">
                    <label for="" class="control-label">No Telepon</label>
                    <input type="text" class="form-control" name="user_phone" id="user_phone" disabled value="{{ $profile->user_phone }}" placeholder="Masukan no telepon aktif Anda">
                    <span class="text-danger">* No telepon hanya bisa diubah melalui aplikasi.</span>
                </div>
                <div class="form-group">
                    <label for="" class="control-label">Alamat</label>
                    <textarea class="form-control" name="user_address" id="user_address" cols="30" rows="5">{{ $profile->user_address }}</textarea>
                </div>
                <button class="btn btn-warning text-white" type="submit">Simpan</button>
            </form>
        </div>
    </div>
@endsection