<html>
    <head>
        <!-- Title -->
        <title>Login - Jaringan BOS</title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}" />

        <style>
            
            .btn-bos-orange {
                color: #fff;
                background-color: #ff822e;
                border-color: #ff822e;
            }

            .btn-bos-orange:hover {
                color: #fff;
                background-color: #FF944B;
                border-color: #FF944B;
            }

            .btn-bos-orange:focus, .btn-bos-orange.focus {
                box-shadow: 0 0 0 0.2rem rgba(222, 170, 12, 0.5);
            }

            .btn-bos-orange.disabled, .btn-bos-orange:disabled {
                color: #fff;
                background-color: #ff822e;
                border-color: #ff822e;
            }

            .btn-bos-orange:not(:disabled):not(.disabled):active, .btn-bos-orange:not(:disabled):not(.disabled).active,
            .show > .btn-bos-orange.dropdown-toggle {
                color: #fff;
                background-color: #d39e00;
                border-color: #c69500;
            }

            .btn-bos-orange:not(:disabled):not(.disabled):active:focus, .btn-bos-orange:not(:disabled):not(.disabled).active:focus,
            .show > .btn-bos-orange.dropdown-toggle:focus {
                box-shadow: 0 0 0 0.2rem rgba(222, 170, 12, 0.5);
            }
        </style>
    </head>
    <body>
        {{-- Image --}}
        <div class="text-center mt-5 mb-4">
            <img src="{{ asset('images/logo-bos.jpg') }}" alt="LOGO BOS" width="200px" />
        </div>

        <div class="col-sm-4 offset-sm-4">
            {{-- ALERT --}}
            @if(Session::has('status') && Session::get('status') == "err")
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>Gagal Masuk !</strong> <br> {{ Session::get('msg') }}
                </div>
            @endif

            {{-- FORM --}}
            <form action="{{ url('/login') }}" method="POST">
                @csrf
                <div class="form-group">
                    <input type="text" class="form-control" required="true" placeholder="NO TELEPON" name="phone" id="phone"/>
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" required="true" placeholder="PIN" maxlength="6" name="pin" id="pin"/>
                </div>
                <div class="form-group">
                    <input type="checkbox" name="show-pin" id="show-pin" onclick="showPin()"/>
                    Lihat PIN
                </div>
                <button class="btn btn-bos-orange btn-block text-white">MASUK</button>
            </form>
        </div>

        {{-- COPYRIGHT --}}
        <div id="copyright" class="col-sm-12 text-center text-warning">
            <span>&copy; 2019. Sistem Informasi Jaringan BOS</span>
        </div>
        
        {{-- SCRIPT --}}

        <!-- Bootstrap JS -->
        <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>

        <!-- JQuery -->
        <script src="{{ asset('js/jquery.js') }}"></script>
        <script>
            function showPin() {
                var x = document.getElementById("pin");
                if (x.type === "password") {
                    x.type = "text";
                } else {
                    x.type = "password";
                }
            }

            $(document).ready(function() {
                //called when key is pressed in textbox
                $("#pin").keypress(function(e) {
                    //if the letter is not digit then display error and don't type anything
                    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                        return false;
                    }
                });
                $("#phone").keypress(function(e) {
                    //if the letter is not digit then display error and don't type anything
                    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                        return false;
                    }
                });
            });
        </script>
    </body>
</html>
