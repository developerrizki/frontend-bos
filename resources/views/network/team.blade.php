@extends('layouts.root')

@section('title', 'TIM SAYA')

@section('content')
    <div class="row">
        <div class="col-sm-3">
            <div class="card">
                <div class="card-body text-center">
                    <h5 class="card-title">Kiri</h5>
                    <h6 class="card-subtitle mb-2 text-muted">{{ $counting['left'] }} Orang</h6>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="card">
                <div class="card-body text-center">
                    <h5 class="card-title">Tengah</h5>
                    <h6 class="card-subtitle mb-2 text-muted">{{ $counting['mid'] }} Orang</h6>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="card">
                <div class="card-body text-center">
                    <h5 class="card-title">Kanan</h5>
                    <h6 class="card-subtitle mb-2 text-muted">{{ $counting['right'] }} Orang</h6>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="card">
                <div class="card-body text-center">
                    <h5 class="card-title">Total </h5>
                    <h6 class="card-subtitle mb-2 text-muted">{{ $counting['total'] }} Orang</h6>
                </div>
            </div>
        </div>
    </div> <br>
    <div class="row">
        <div class="col-sm-12">
            <h5>Data Downline Level 1</h5> <br>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <th width="5%">No</th>
                        <th width="50%">Nama</th>
                        <th width="45%">Telepon</th>
                    </thead>
                    <tbody>
                        @if(sizeof($downline['level1']) > 0)
                            @foreach($downline['level1'] as $key => $result)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ ucwords(strtolower($result->user_name)) }}</td>
                                    <td>{{ substr_replace($result->user_phone, 'xxxx', -4) }}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="3">Data Downline Level 1 Tidak Tersedia.</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-sm-12">
            <br> <h5>Data Downline Level 2</h5> <br>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <th width="5%">No</th>
                        <th width="50%">Nama</th>
                        <th width="45%">Telepon</th>
                    </thead>
                    <tbody>
                        @if(sizeof($downline['level2']) > 0)
                            @foreach($downline['level2'] as $key => $result)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ ucwords(strtolower($result->user_name)) }}</td>
                                    <td>{{ substr_replace($result->user_phone, 'xxxx', -4) }}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="3">Data Downline Level 2 Tidak Tersedia.</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-sm-12">
            <br> <h5>Data Downline Level 3</h5> <br>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <th width="5%">No</th>
                        <th width="50%">Nama</th>
                        <th width="45%">Telepon</th>
                    </thead>
                    <tbody>
                        @if(sizeof($downline['level3']) > 0)
                            @foreach($downline['level3'] as $key => $result)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ ucwords(strtolower($result->user_name)) }}</td>
                                    <td>{{ substr_replace($result->user_phone, 'xxxx', -4) }}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="3">Data Downline Level 3 Tidak Tersedia.</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection