<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/upline', 'UplineController@index');
Route::get('/register-downline', 'UplineController@registerDownline');

Route::get('/', 'NetworkController@index');
Route::post('/login', 'NetworkController@login');
Route::get('/dasbor', 'NetworkController@dashboard');
Route::get('/profil', 'NetworkController@profile');
Route::post('/profil', 'NetworkController@profile');
Route::get('/tim', 'NetworkController@team');
Route::get('/bonus', 'NetworkController@bonus');
Route::get('/logout', 'NetworkController@logout');
