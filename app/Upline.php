<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upline extends Model
{
    protected $table = "bos_sys_user";

    function getLastNumber()
    {
        return Upline::where('user_level_id',3)
                    ->orderBy('user_referal_use_number','DESC')
                    ->first();
    }

    function lazyCallDownline()
    {
        yield Upline::where('user_balance','>=',0)
                    ->where('user_level_id',3)
                    ->whereNull('user_upline')
                    ->whereNotNull('user_referal_use_code')
                    ->where('user_referal_use_code','!=','')
                    ->where('user_id','!=',1)
                    ->orderBy('created_time','DESC')
                    ->limit(10)
                    ->get();
    }

    function getDownline($user_referal_code){
        yield Upline::where('user_referal_use_code', $user_referal_code)
                ->orderBy('user_referal_use_number','ASC')->get();
    }
    
    function customRange($code){
        yield Upline::whereIn('user_referal_use_code', $code)
                    ->orderBy('user_referal_use_number','ASC')->get();
    }
}
