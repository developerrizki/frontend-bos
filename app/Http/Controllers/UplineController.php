<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Upline;

ini_set('max_execution_time', 0);

class UplineController extends Controller
{
    public function __construct()
    {
        $this->upline = new Upline;
    }

    public function index(Request $request)
    {
        $users = $this->upline->lazyCallDownline();

        foreach ($users as $user) {
            foreach ($user as $key => $value) {
                $request->session()->forget('arr_downline');

                $upline 		= $value->user_referal_use_code == "" ? '081289895796' : $value->user_referal_use_code;
                // $upline 		= $value->user_referal_use_code;
                $downline		= $value->user_phone;
                
                $arr_downline   = array();
                
                $last_number	= $this->upline->getLastNumber();
                $treeNumber     = $last_number ? $last_number->user_referal_use_number + 1 : 1;
                $treeUpline     = round($treeNumber / 3);

                $detail_upline  = Upline::where('user_phone', $upline)->first();
    
                $arr_downline = array();            
                $detail_downline = Upline::where('user_phone', $downline)->first();
                $count_downline = Upline::where('user_referal_use_code', $detail_upline->user_referal_code)->count();
                
                if($count_downline < 3){
                    
                    $value 	= array(
                        'user_referal_use_code' => $detail_upline->user_referal_code,
                        'user_referal_use_number' => $treeNumber,
                        'user_upline' => $detail_upline->user_referal_use_number,
                    );
        
                    \DB::table('bos_sys_user')->where('user_phone', $downline)->update($value);           
                    
                    dump($downline);
                    break;
                    
                } else {
                    $get_downline = Upline::where('user_referal_use_code', $detail_upline->user_referal_code)
                                        ->orderBy('user_referal_use_number','ASC')->get();

                    foreach ($get_downline as $key => $value) {
                        $result = $this->setDownline($value->user_phone, $downline);

                        if($result == true){
                            dump($downline);
                            break;
                        } else {
                            $arr_downline[] = $value->user_referal_code;
                        }
                    }
                }
                
                // dump($arr_downline);

                if(!empty($arr_downline) && sizeof($arr_downline) == 3){
                    $this->loopDownline($arr_downline, $downline);
                }
            }
        }
    }

    public function setDownline($upline, $downline)
    {
        $last_number	= $this->upline->getLastNumber();
        $treeNumber     = $last_number ? $last_number->user_referal_use_number + 1 : 1;
        $treeUpline     = round($treeNumber / 3);

        // dump($last_number->user_referal_use_number);
    
        $detail_upline  = Upline::where('user_phone', $upline)->first();
        $detail_downline    = Upline::where('user_phone', $downline)->first();
        $count_downline     = Upline::where('user_referal_use_code', $detail_upline->user_referal_code)->count();
        
        if($count_downline < 3){

            $value 	= array(
                'user_referal_use_code' => $detail_upline->user_referal_code,
                'user_referal_use_number' => $treeNumber,
                'user_upline' => $detail_upline->user_referal_use_number,
            );

            \DB::table('bos_sys_user')->where('user_phone', $downline)->update($value);

            return true;
        } else {
            return false;
        }
    }

    function loopDownline($array_downline, $downline){

        $get_downline = $this->upline->customRange($array_downline);
        $arr_downline   = array();

        foreach ($get_downline as $item) {
            foreach ($item as $key => $value) {
                $result = $this->setDownline($value->user_phone, $downline);

                if($result == true){
                    dump($downline);
                    break;
                } else {
                    $arr_downline[] = $value->user_referal_code;
                }
            }
        }
        
        // dump($arr_downline);

        if(!empty($arr_downline) && sizeof($arr_downline) == sizeof($array_downline)*3){
            $this->loopDownline($arr_downline, $downline);
        }
    }

    public function registerDownline(Request $request)
    {
        $upline 		= $request->upline;
        $downline		= $request->downline;
        
        $arr_downline   = array();
        
        $last_number	= $this->upline->getLastNumber();
        $treeNumber     = $last_number ? $last_number->user_referal_use_number + 1 : 1;
        $treeUpline     = round($treeNumber / 3);

        $detail_upline  = Upline::where('user_phone', $upline)->first();

        $arr_downline = array();            
        $detail_downline = Upline::where('user_phone', $downline)->first();
        $count_downline = Upline::where('user_referal_use_code', $detail_upline->user_referal_code)->count();
        
        if($count_downline < 3){
            
            $value 	= array(
                'user_referal_use_code' => $detail_upline->user_referal_code,
                'user_referal_use_number' => $treeNumber,
                'user_upline' => $detail_upline->user_referal_use_number,
            );

            \DB::table('bos_sys_user')->where('user_phone', $downline)->update($value);           
            
            dump($downline);
        } else {
            $get_downline = Upline::where('user_referal_use_code', $detail_upline->user_referal_code)
                                ->orderBy('user_referal_use_number','ASC')->get();

            foreach ($get_downline as $key => $value) {
                $result = $this->setDownline($value->user_phone, $downline);

                if($result == true){
                    dump($downline);
                    break;
                } else {
                    $arr_downline[] = $value->user_referal_code;
                }
            }
        }

        if(!empty($arr_downline) && sizeof($arr_downline) == 3){
            $this->loopDownline($arr_downline, $downline);
        }
    }
}
