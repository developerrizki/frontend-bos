<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Network;

class NetworkController extends Controller
{
    public function __construct()
    {
        $this->network = new Network;
    }

    public function index(Request $request)
    {
        //check auth
        if($request->session()->has('auth')){
            return redirect('dasbor');
        }

        return view('network.login');
    }

    public function login(Request $request)
    {
        $phone = $request->phone;
        $pin   = $this->network->hashing($request->pin);
        
        $check_phone = Network::where('user_username', $phone)->first();
        
        if($check_phone){
            $check_pin = Network::where('user_username', $phone)->where('user_password', $pin)->first();

            if($check_pin){
                //session put array
                $auth = array(
                    'id'        => $check_pin->user_id,
                    'name'      => $check_pin->user_name,
                    'phone'     => $check_pin->user_phone,
                    'username'  => $check_pin->user_username,
                    'email'     => $check_pin->user_email,
                    'level'     => $check_pin->user_level_id,
                );

                $request->session()->put('auth', $auth);

                $request->session()->flash('status','success');
                $request->session()->flash('msg','Selamat Anda berhasil Masuk.');
                return redirect('/dasbor');    
            }

            $request->session()->flash('status','err');
            $request->session()->flash('msg','Pin yang Anda masukan salah.');
            return redirect('/');
        }

        $request->session()->flash('status','err');
        $request->session()->flash('msg','No telepon Anda tidak terdaftar dalam sistem kami.');
        return redirect('/');
    }

    public function dashboard(Request $request)
    {
        //check auth
        if(!$request->session()->has('auth')){
            return redirect('/');
        }

        //get session auth
        $auth    = $request->session()->get('auth');
        $profile = Network::find($auth['id']);
        $upline  = $profile->user_referal_use_code != "" ? $this->network->upline($profile->user_referal_use_code) : "";

        return view('network.dashboard', compact('profile','upline'));
    }

    public function profile(Request $request)
    {
        //check auth
        if(!$request->session()->has('auth')){
            return redirect('/');
        }

        //get session auth
        $auth    = $request->session()->get('auth');
        $profile = Network::find($auth['id']);

        //if send post
        if($request->method() == "POST"){
            //do something
            $profile                = Network::find($auth['id']);
            $profile->user_email     = $request->user_email;
            $profile->user_address   = $request->user_address;
            $profile->save();

            $request->session()->flash('status','success');
            $request->session()->flash('msg','mengubah data profil.');
            return redirect('/profil');    
        }

        return view('network.profile', compact('profile','upline'));
    }

    public function team(Request $request)
    {
        //check auth
        if(!$request->session()->has('auth')){
            return redirect('/');
        }

        //get session auth
        $auth    = $request->session()->get('auth');
        $profile = Network::find($auth['id']);

        //downline level 1
        $level1 = Network::where('user_referal_use_code', $profile->user_referal_code)->orderBy('user_referal_use_number','ASC')->get();
        $level2 = array();
        $level3 = array();
        
        //if downline level 1 not empty
        if($level1){
            //downline level 2
            $referal_lvl_one = Network::where('user_referal_use_code', $profile->user_referal_code)->pluck('user_referal_code');
            $level2 = Network::whereIn('user_referal_use_code', $referal_lvl_one)->orderBy('user_referal_use_number','ASC')->get();

            //if downline level 2 not empty
            if($level2) {
                //downline level 3
                $referal_lvl_two = Network::whereIn('user_referal_use_code', $referal_lvl_one)->pluck('user_referal_code');
                $level3 = Network::whereIn('user_referal_use_code', $referal_lvl_two)->orderBy('user_referal_use_number','ASC')->get();
            }
        }

        //counting member downline
        $countLeft     = 0;
        $countMid      = 0;
        $countRight    = 0;

        $left   = isset($level1[0]) ? $level1[0]->user_referal_code : "";
        $mid    = isset($level1[1]) ? $level1[1]->user_referal_code : "";
        $right  = isset($level1[2]) ? $level1[2]->user_referal_code : "";

        //if value left not null
        if($left != ""){
            $countLeft = $this->network->countDownline($left);
        }
        
        //if value mid not null
        if($mid != ""){
            $countMid = $this->network->countDownline($mid);
        }
        
        //if value right not null
        if($right != ""){
            $countRight = $this->network->countDownline($right);
        }

        //downline
        $downline = array(
            'level1' => $level1,
            'level2' => $level2,
            'level3' => $level3
        );

        //counting
        $counting = array(
            'left'  => $countLeft,
            'mid'   => $countMid,
            'right' => $countRight,
            'total' => $countLeft + $countMid + $countRight
        );

        return view('network.team', compact('profile', 'downline','counting'));
    }

    public function bonus(Request $request)
    {
        //check auth
        if(!$request->session()->has('auth')){
            return redirect('/');
        }

        //get session auth
        $auth    = $request->session()->get('auth');
        $profile = Network::find($auth['id']);

        //periode
        $periode = array(
            'now' => date('Y-m-d'),
            'sevenDay' => date('Y-m-d', strtotime(date('Y-m-d'). '-7 day')),
            'oneMonth' => date('Y-m-d', strtotime(date('Y-m-d'). '-30 day')),
            'threeMonth' => date('Y-m-d', strtotime(date('Y-m-d'). '-90 day'))
        );

        //getTransaksi
        $periodeDate = isset($request->periode) ? $request->periode : "";
        $transaction = $this->network->getHistory($periodeDate, $profile);

        $history = $transaction['history'];
        $total = $transaction['total'];

        return view('network.bonus', compact('profile','periode','history','total'));
    }

    public function logout(Request $request)
    {
        //delete session
        $request->session()->forget('auth');
        return redirect('/');
    }
}
