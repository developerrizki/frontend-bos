<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Network extends Model
{
	protected $table = "bos_sys_user";
	protected $primaryKey = "user_id";
	public $timestamps = false;

    function hashing($password){

		$hash = hash('sha256', md5($password));
		$hash = md5(sha1($hash));

		return $hash;
	}

	function upline($referal)
	{
		$upline = Network::where('user_referal_code', $referal)->first();
		return $upline;
	}

	function getHistory($periode, $profile)
	{
		$now 			= date('Y-m-d');
		$userId 		= array();

		//get data history transaction from upline
		if(!empty($profile->user_referal_use_code)){
			$userId[]		= Network::where('user_referal_code', $profile->user_referal_use_code)->first()->user_id;
		}

		//get data history transaction from downline
		if(isset($profile->user_referal_code)){
			$level1 = Network::where('user_referal_use_code', $profile->user_referal_code)->get();

			if($level1){
				$referal = array();

				foreach ($level1 as $key => $value) {
					$userId[] = $value->user_id;
					$referal[] = $value->user_referal_code;
				}

				if(sizeof($referal) != 0) {
					$data  = $this->getUserId($referal, $userId, 3);

					for ($i=0; $i < sizeof($data['userId']) ; $i++) { 
						$userId[] = $data['userId'][$i];
					}

					for ($i=0; $i < sizeof($data['referal']) ; $i++) { 
						$referal[] = $data['referal'][$i];
					}
				}
			}
		}


		$transaction 	= DB::table('bos_transaction')
							->select('transaction_id','transaction_desc','bos_transaction.created_time','user_referal_code')
							->join('bos_sys_user', 'bos_transaction.user_id', '=', 'bos_sys_user.user_id')
							->whereIn('user_referal_code', $referal)
							->orderBy('bos_transaction.created_time','DESC');

		if($periode < '2019-08-28') {
			$periode = '2019-08-28';
		}

		$transaction = $transaction->whereBetween(\DB::raw('DATE(bos_transaction.created_time)'), [$periode, $now]);

		$data['history'] 	= $transaction->get();
		$data['total'] 		= $transaction->count() * 5;

		return $data;
	}

 	function getUserId($referal, $userId, $limit)
	{
		if(isset($referal) && $limit <= 12){
			$nextLevel = Network::whereIn('user_referal_use_code', $referal)->get();

			if($nextLevel){
				$referal = array();

				foreach ($nextLevel as $key => $value) {
					$userId[] = $value->user_id;
					$referal[] = $value->user_referal_code;
				}

				if(sizeof($referal) > 0) {
					$limit = $limit + 1;
					$data  = $this->getUserId($referal, $userId, $limit);

					for ($i=0; $i < sizeof($data['userId']) ; $i++) { 
						$userId[] = $data['userId'][$i];
					}

					for ($i=0; $i < sizeof($data['referal']) ; $i++) { 
						$referal[] = $data['referal'][$i];
					}
				}
			}
		}

		$data = array(
			'userId' => $userId,
			'referal' => $referal
		);

		return $data;
	}

	function countDownline($referal)
	{
		$count 			= Network::where('user_referal_use_code', $referal)->count();
		$count_next 	= 0;
		$next_referal 	= array();

		//cek next downline
		$downline_referal = Network::where('user_referal_use_code', $referal)->pluck('user_referal_code');

		if(sizeof($downline_referal) > 0){
			for ($i=0; $i < sizeof($downline_referal); $i++) { 
				$count_next = $this->countNextDownline($downline_referal[$i]);
				$count = $count + $count_next;
			}
		}
		return $count;
	}

	function countNextDownline($referal)
	{
		//count
		$count_next = 0;
		$data 		= Network::where('user_referal_use_code', $referal)->get();
		$count 		= sizeof($data);
		
		//cek next downline
		foreach ($data as $key => $value) {
			//cek next downline
			$downline_referal = Network::where('user_referal_use_code', $value->user_referal_code)->pluck('user_referal_code');

			if(sizeof($downline_referal) > 0){
				for ($i=0; $i < sizeof($downline_referal); $i++) { 
					$count_next = $this->countNextDownline($downline_referal[$i]);
					$count = $count + $count_next;
				}
			}
		}
		
		return $count;
	}
}
